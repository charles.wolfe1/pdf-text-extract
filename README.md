Setup:
brew install poppler
pip install -r requirements.txt
python -m spacy download en_core_web_sm

To start 'webserver':
jupyter notebook

then choose 'PDF_Text_Extractor' and run it